package edu.missouristate.services;

public interface ExampleService {

	public boolean isAuthenticated(String id, String password);

}
