package edu.missouristate.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.missouristate.services.ExampleService;

@Controller
public class ExampleController {
	
	@Autowired
	ExampleService exampleService;
	
    @GetMapping(value = { "/examplemvc" })
    public String getDemoPage(HttpServletRequest request, HttpSession session, 
    		Model model, @RequestParam("id")String id, 
    		@RequestParam("password")String password) {
    	
    	if (id == null && password == null) {
    		return "example";
    	}
    	
    	boolean isAuthenticated = exampleService.isAuthenticated(id, password);
    	
    	if (isAuthenticated) {
    		session.setAttribute("authenticated", "true");
    		return "redirect:/dashboard";
    	} else {
    		model.addAttribute("message", "Sorry, invalid credentials!");
    		return "example";
    	}
    }

	@GetMapping("/dashboard")
	public String getHome(HttpSession session) {
		if (session.getAttribute("authenticated") != null) {
			return "dashboard";	
		} else {
			return "example";
		}
	}
	
}
