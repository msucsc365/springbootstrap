package edu.missouristate.dao;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.stereotype.Repository;

import edu.missouristate.model.Credentials;

@Repository
public class ExampleRepository {
	
	// Global Member Variables
	List<Credentials> credentialsList = new ArrayList<Credentials>();
	
	@PostConstruct
	private void postConstruct() {
		intializeDatasource();
	}
	
	private void intializeDatasource() {
		Credentials creds = new Credentials();
		creds.setId("admin");
		creds.setPassword("password");		
		credentialsList.add(creds);
	}

	public List<Credentials> getCredentialsList() {
		// Normally this would be a database call...
		// SELECT * FROM <TABLE>;
		return credentialsList;
	}

	/**
	 * If credentials not found, always returns null
	 * @param id
	 * @param password
	 * @return
	 */
	public Credentials getCredentialsList(String id, String password) {
		// Normally this would be a database call...
		// SELECT * FROM TABLE WHERE ID = id AND password = 'password';
		Credentials resultCreds = null;
		
		if (id == null || id.length() == 0 || password == null || password.length() == 0) 
			return resultCreds;
		
		for (Credentials creds : credentialsList) {
			if (creds.getId().equalsIgnoreCase(id) && creds.getPassword().equals(password)) {
				return creds;
			}
		}
		
		return resultCreds;
	}
	
	
}
